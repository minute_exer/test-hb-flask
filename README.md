Tested with Python 3.8 and 3.9

run:
./docker_start.sh

browse to:
http://localhost:56733/v

you will not get HoneyBadger messages


get into the docker:
docker exec -it similarity_cache_server_docker bash

install curl:
apk add curl

check connection
curl -X POST https://api.honeybadger.io/v1/notices 

If you run this code without a docker, it will work
what am I missing?