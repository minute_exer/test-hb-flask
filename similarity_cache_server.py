# similarity_cache_server.py
import json
import logging
import os

from flask import Flask, request, abort
from honeybadger.contrib import FlaskHoneybadger
from honeybadger import honeybadger


app = Flask(__name__)


handler = logging.FileHandler("app.log")  # Create the file logger
handler.setFormatter(logging.Formatter('[%(asctime)s] %(levelname)s in %(module)s: %(message)s'))
app.logger.addHandler(handler)             # Add it to the built-in logger
app.logger.setLevel(logging.DEBUG)         # Set the log level to debug

with app.app_context():
    app.logger.info("Initializing Env: " + os.environ['FLASK_ENV'])
    app.config['HONEYBADGER_ENVIRONMENT'] = os.environ['FLASK_ENV']
    app.config['HONEYBADGER_API_KEY'] = 'a294984f'
    FlaskHoneybadger(app, report_exceptions=True)



@app.route("/v")
def version():
    print("forcing")
    honeybadger.notify(error_class='ValueError', error_message='Something bad happened!')
    print("Dividing")
    v = 4/0
    return "1.0 - 20/4/2021"


# running the server
# app.run(debug=False)  # to allow for debugging and auto-reload

if __name__ == '__main__':
    app.run()
