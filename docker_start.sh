#!/bin/bash
app="similarity_cache_server_docker"
docker build -t ${app} .
docker run -d -p 56733:80 \
  --name=${app} \
  -e FLASK_ENV=qa3 \
  -v $PWD:/app ${app}