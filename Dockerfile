FROM tiangolo/uwsgi-nginx:python3.8-alpine
RUN apk --update add bash nano
RUN apk --update add gcc libc-dev g++ libffi-dev libxml2 libffi-dev unixodbc-dev
ENV STATIC_URL /static
ENV STATIC_PATH /var/www/app/static
RUN pip3 install --no-cache --upgrade pip setuptools
RUN mkdir -p /var/www/app/
WORKDIR /var/www/app/
COPY . /var/www/app/
RUN pip install -r /var/www/app/requirements.txt
